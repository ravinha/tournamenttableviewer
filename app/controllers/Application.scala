package controllers

import models.strategy.Tree.{Game, EliminationTree}
import models.strategy.VSet
import models.strategy.scores.VolleyballScore
import models.strategy.strategies.DoubleEliminationStrategy
import models.team.Team
import models.team.teams.volleyball.volleyballs.VolleyballTeam
import play.api._
import play.api.data.Forms._
import play.api.mvc._
import play.api.data._

import play.api.libs.json._

import scala.concurrent.Future


object Application extends Controller {


  var teams = List[VolleyballTeam]()
  val strategy = DoubleEliminationStrategy()
  var tree = new EliminationTree()

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def register() = Action(BodyParsers.parse.json) {

      implicit request =>
        request.body.validate[VolleyballTeam].map {
          team => {
              teams=team::teams
          }
        }
      Ok("")
  }

  def submitStrategy() = Action {
    tree = strategy.generateTree(teams)
    strategy.populateTree(tree,teams)
    Ok("Tree populated")
  }

  var actualGame = new Game()

//  def setGame() = Action(BodyParsers.parse.json){
//    implicit request =>
//      request.body.validate[Game].map {
//        game => {
//          actualGame=game
//        }
//      }
//    Ok("Game set")
//  }
//
//  val VSetw = new VSet()
//  VSetw.won=true
//  val VSetl = new VSet()
//  VSetl.won=false
//  val setsW = List(VSetw,VSetw,VSetw)
//  val setsL = List(VSetl,VSetl,VSetl)
//
//  val score:VolleyballScore = VolleyballScore()
//  val score2:VolleyballScore = VolleyballScore()
//  score.sets=setsW              //winning score
//  score2.sets=setsL             //losing score
//
//
//
//  def setWinner() = Action(BodyParsers.parse.json){
//    implicit request =>
//      request.body.validate[String].map {
//        team => {
//
//          if (actualGame.value.host == team){
//            actualGame.value.scoreHost = score
//            actualGame.value.scoreGuest = score2
//          }
//          else {
//            actualGame.value.scoreHost = score2
//            actualGame.value.scoreGuest = score
//          }
//        }
//          strategy.updateTree(tree)
//      }
//      Ok("Winner set")
//  }

//  def sendTree = Action(
//    Ok(tree)
//  )

}