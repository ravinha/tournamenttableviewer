package models.team.teams.volleyball.volleyballs


import models.team.teams.volleyball.VolleyballTeams
import play.api.libs.json._

/**
 * Created by Szymek.
 */
class VolleyballTeam( val _id:String,
                      val name:String) extends VolleyballTeams {

}
object VolleyballTeam{
  def apply(name:String):VolleyballTeam = new VolleyballTeam(name, name)
  def unapply(v:VolleyballTeam) = {
    if (v != null) Some(v.name) else None
  }

  implicit val VolleyballTeamWrites = Json.writes[VolleyballTeam]
  implicit val VolleyballTeamReads = Json.reads[VolleyballTeam]

}