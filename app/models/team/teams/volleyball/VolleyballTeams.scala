package models.team.teams.volleyball

import models.team.Team


/**
 * Created by Szymek.
 */
trait VolleyballTeams extends Team {

  val _id:String
  val name:String

}
