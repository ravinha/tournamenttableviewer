package models.strategy.scores

/**
 * Created by Rafal on 2014-12-12.
 */
class MatchNotFinishedException(msg:String=null, cause:Throwable=null) extends java.lang.Exception (msg, cause){}
