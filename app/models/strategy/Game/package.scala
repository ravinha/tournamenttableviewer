package models.strategy

import play.api.libs.json.Json


/**
 * Created by ludwik on 13.12.14.
 */
package object Tree {
   class Game(var right : Game, var left:Game,var parent:Game, var value:Match){
     def this(){
       this(null,null,null,null)
     }
   }

  object Game{
    def apply(right : Game,left:Game, parent:Game,value:Match):Game =
      new Game(right,left,parent,value)
      def unapply(v:Game) = {
        if (v != null) Some(v.right,v.left,v.parent,v.value) else None
      }
//
//    implicit val GameWrites = Json.writes[Game]
//    implicit val GameReads = Json.reads[Game]
  }

   class EliminationTree(val root: Game){
    def this(){
       this(new Game(null,null,null,null))
    }
  //  def getLatest():Game = {return new Game();}
  }
}

