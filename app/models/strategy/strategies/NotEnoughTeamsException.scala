package models.strategy.strategies

/**
 * Created by Rafal on 2014-12-13.
 */
class NotEnoughTeamsException(msg:String=null, cause:Throwable=null) extends java.lang.Exception (msg, cause){}
